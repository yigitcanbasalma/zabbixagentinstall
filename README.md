### Yapısı ve Çalışma Prensibi
Scriptin ana amacı, büyük sunucu gruplarının yer aldığı yapılara, hızlı, merkezi ve hataya mahal vermeden kurulumları tamamlamaktır. Scriptin doğru çalışabilmesi için, sunucuların hostname bilgisinin anlamlı şekilde girilmesi gerekmektedir. Örnek olarak "yccbapp01p.local" ismini ele alırsak, buradan çıkaracağımız iki adet bilgi var. İlk olarak uygulama tipi, ikinci olarak da uygulamanın ortam bilgisi. Bunu da aşağıdaki gibi konfigürasyon düzenleyerek sağlayacağız.
```python
metadata_config = {
    "environment": {
        "d": "Test",
        "q": "Qa",
        "s": "Staging",
        "p": "Production"
    },
    "app_type": {
        ("cbapp", "ccapp"): "Couchbase Cache Application"
    }
}
```

Konfigürasyona bakarak işlemi yapacak olan fonksiyon "get_app_type()" isimli fonksiyondur. Detaylı olarak inceleyebilirsiniz.


### Kullanıcı Tanımlı Scriptler
Zabbix içerisinde, kendi hazırladığımız scriptleri birer key olarak tanıtabiliyoruz. "configs/*.conf" dosyaları da tam olarak bu işe yaramakta. Ben burayı iki bölüme ayırdım. Takip etmesi daha kolay olacaktır. Zabbix agent konfigürasyonu içerisinde de aşağıdaki gibi import edilerek kullanılmakta. "$UserParametersConfBase" parametresi, script içerisinde verilen lokasyon ile değiştirilmektedir.

```bash
Include=$UserParametersConfBase/*.conf
```

### Script Sonucu Oluşan Dosyalar
* __"/etc/environment":__ Ortam bilgisinin tam hali yazılır. Örn. p => Production
* __"/etc/tags":__ Script içerisindeki konfigürasyon alanında "tag" özelliği "True" olarak işaretlenen bilgiler bu dosyaya yazılır. Örnek aşağıdadır.
```
"host_metadata": {
    "os_type": {
        "name": os.uname()[0],
        "write_file": None,
        "tag": False
    },
    "os_info": {
        "name": "{0}-{1}".format(dist_name, dist_version),
        "write_file": None,
        "tag": False
       },
    "app_type": {
        "name": get_app_type(),
        "write_file": None,
        "tag": True
    },
    "environment": {
        "name": metadata_config["environment"][gethostname().split(".")[0][-1]],
        "write_file": "/etc/environment",
        "tag": False
    }
}
```

## Uyarılar
* __Scriptin Kapasitesi:__ "Scriptin Yapısı" alanında bahsedilen tüm fonksiyonlar aktif olarak çalışır durumdadır. Eksikleri muhakkak çıkacak ve bu scriptin daha da geliştirilmesi gerekliliği ortaya çıkacaktır. Bunun için desteğinizi esirgememenizi temenni ediyorum =)
* __"HostMetadata" Alanı:__ Bu alan standart olarak 255 karakter olarak gelmektedir. Zabbix uygulamasının database'ine bağlanıp bu alanı ihtiyacınıza göre büyütebilirsiniz.