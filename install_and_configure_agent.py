#!/usr/bin/python
# -*- coding: utf-8 -*-

#   Install agent automatically and set necessary configuration.

import sys
import os
import logging
import re
import argparse

from socket import gethostname
from subprocess import Popen, PIPE
from hashlib import md5
from platform import dist
from time import sleep

__author__ = "Yiğit Can BAŞALMA"
__email__ = "yigit.basalma@gmail.com"

metadata_config = {
    "environment": {
        "d": "Test",
        "q": "Qa",
        "s": "Staging",
        "p": "Production"
    },
    "app_type": {
        ("<hostname_part_for_application_type>", ): "<application_type_explain>"
    }
}


dist_name, dist_version, _ = dist()


def get_app_type():
    host = gethostname().split(".")[0]
    app_type = None
    for k, v in metadata_config["app_type"].iteritems():
        if bool(re.search("({0})".format("|".join(k)), host)):
            app_type = v
    if not app_type:
        app_type = "Other"
    return app_type


script_config = {
    "target_hostname": gethostname(),
    "host_metadata": {
        "os_type": {
            "name": os.uname()[0],
            "write_file": None,
            "tag": False
        },
        "os_info": {
            "name": "{0}-{1}".format(dist_name, dist_version),
            "write_file": None,
            "tag": False
        },
        "app_type": {
            "name": get_app_type(),
            "write_file": None,
            "tag": True
        },
        "environment": {
            "name": metadata_config["environment"][gethostname().split(".")[0][-1]],
            "write_file": "/etc/environment",
            "tag": False
        }
    },
    "agent_rpms": {
        ("centos", "redhat"): {
            "7": "https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-1.el7.noarch.rpm",
            "6": "https://repo.zabbix.com/zabbix/4.0/rhel/6/x86_64/zabbix-release-4.0-1.el6.noarch.rpm"
        }
    },
    "tags_path": "/etc/tags",
    "sample_config_path": os.path.join(os.getcwd(), "configs"),
    "user_parameters_conf_base": "/etc/zabbix/zabbix_agentd.d/configs",
    "user_parameters_script_base": "/etc/zabbix/zabbix_agentd.d"
}


def arg_parser():
    parser = argparse.ArgumentParser(description="Zabbix Agent Installer")
    parser.add_argument("-H", dest="from_hosts", action="store", required=True, help="Zabbix proxy/manager hosts IP addresses.(Comma sep if more than one)")
    parser.add_argument("--silent", dest="silent_mode", action="store_true", default=False, help="Run script with silent mode and write output to the log file.(/tmp/zabbix_agent_install.log)")
    parser.add_argument("--re-configure", dest="re_configure", action="store_true", default=False, help="Re-Configure all configuration.")
    parser.add_argument("--add-repos", dest="add_repos", action="store_true", help="Add official Zabbix repos.")
    parser.add_argument("--configure-conf-files", dest="configure_conf_files", action="store_true", help="Re-configure LLD and UserParameters config files.")
    parser.add_argument("--chown", dest="chown", action="store_true", help="Change owner the /etc/zabbix/zabbix_agentd.d folder to zabbix user.")
    return parser.parse_args()


def configure_logger(args):
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - '%(message)s'", datefmt="%d/%m/%Y %H:%M:%S")
    lg = logging.getLogger("Zabbix Agent Installer")
    lg.setLevel(logging.INFO)
    if args.silent_mode:
        file_handler = logging.FileHandler(filename="/tmp/zabbix_agent_install.log")
        file_handler.setFormatter(formatter)
        lg.addHandler(file_handler)
        return lg
    console = logging.StreamHandler()
    console.setFormatter(formatter)
    lg.addHandler(console)
    return lg


def run_local_script(script):
    lp = Popen([script], stderr=PIPE, stdout=PIPE, shell=True)
    output = lp.communicate()
    return lp.returncode, output


def write_file(_path, content):
    t = md5(_path).hexdigest()
    with open(os.path.join("/tmp", t), "w") as _file:
        _file.write(content)
    run_local_script("mv {0} {1}".format(os.path.join("/tmp", t), _path))


def zabbix_agent_not_installed():
    r_code, _ = run_local_script("service zabbix-agent status")
    return r_code != 0


def get_host_metadata():
    host_metadata = list()
    host_tags = list()
    for k, v in script_config["host_metadata"].iteritems():
        if v["tag"]:
            host_tags.append(v["name"])
        host_metadata.append(v["name"])
        logger.info("Metadata configured. Name: '{0}'.".format(v["name"]))
        if v["write_file"] is not None:
            write_file(v["write_file"], v["name"])
            logger.info("Wrote the metadata information to file. File: '{0}'.".format(v["write_file"]))
    metadata = "|".join(host_metadata)
    write_file(script_config["tags_path"], "|".join(host_tags))
    return metadata


def install_package(add_repos=False):
    if add_repos:
        dist_key = None
        for i in script_config["agent_rpms"].keys():
            if dist_name in i:
                dist_key = i
        run_local_script("rpm --replacepkgs -Uvh {0}".format(script_config["agent_rpms"][dist_key][dist_version[0]]))
    install_command = "yum install zabbix-agent -y && chkconfig zabbix-agent on"
    logger.info("Installing dependency. Commands: {0}".format(install_command))
    r_code, out = run_local_script(install_command)
    if r_code == 0:
        if not os.path.exists(script_config["user_parameters_conf_base"]):
            os.makedirs(script_config["user_parameters_conf_base"])
            sleep(0.5)
            logger.info("Packages installed successfully.")
        return True
    logger.error("Getting error when trying to install packages. Error is: '{0}'.".format(out[1]))
    sys.exit(-1)


def agent_restart():
    # Start Agent
    start_agent = "service zabbix-agent restart"
    r_code, out = run_local_script(start_agent)
    if r_code != 0:
        logger.error("Getting error when trying to start zabbix-agent service. Error is: '{0}'.".format(out[1]))
        logger.info("Agent install aborted.")
        sys.exit(-1)
    logger.info("Service started successfully.")


def configure_conf_file():
    # Low Level Discovery Config
    low_level_discovery_sample = open(os.path.join(script_config["sample_config_path"], "general_lld.conf")).read()
    low_level_discovery_sample = low_level_discovery_sample.replace("$UserParametersScriptBase", script_config["user_parameters_script_base"])
    write_file(os.path.join(script_config["user_parameters_conf_base"], "general_lld.conf"), low_level_discovery_sample)
    # Custom Check Config
    general_check_script_conf = open(os.path.join(script_config["sample_config_path"], "general_check.conf")).read()
    general_check_script_conf = general_check_script_conf.replace("$UserParametersScriptBase", script_config["user_parameters_script_base"])
    write_file(os.path.join(script_config["user_parameters_conf_base"], "general_check.conf"), general_check_script_conf)


def configure_agent(args):
    logger.info("Starting to the Zabbix Agent Auto Installer for {0}.".format(script_config["target_hostname"]))
    # Agent Config
    sample_agent_config = open(os.path.join(script_config["sample_config_path"], "sample.cfg"), "r").read()
    sample_agent_config = sample_agent_config.replace("$ZabbixServerIPS", args.from_hosts)
    sample_agent_config = sample_agent_config.replace("$MachineHostname", script_config["target_hostname"])
    sample_agent_config = sample_agent_config.replace("$UserParametersConfBase", script_config["user_parameters_conf_base"])
    sample_agent_config = sample_agent_config.replace("$HostMetadata", get_host_metadata())
    write_file("/etc/zabbix/zabbix_agentd.conf", sample_agent_config)
    # Configure *.conf Files
    configure_conf_file()
    # Move Existing Low Level Discovery Config
    run_local_script("mv /etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf {0}".format(script_config["user_parameters_conf_base"]))
    # Start Agent
    agent_restart()
    logger.info("Agent install completed.")


if __name__ == "__main__":
    config = arg_parser()
    logger = configure_logger(args=config)
    if zabbix_agent_not_installed():
        install_package(add_repos=config.add_repos)
        configure_agent(args=config)
    else:
        if config.re_configure:
            configure_agent(args=config)
        elif config.configure_conf_files:
            configure_conf_file()
    if config.chown:
        run_local_script("chown -R zabbix. /etc/zabbix/zabbix_agentd.d")
